package uz.alphazet.hackathon.core.base;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.MenuItem;

import dagger.android.support.DaggerAppCompatActivity;
import uz.alphazet.hackathon.R;

/**
 * Created by Siyavushkhon on 01.12.2018.
 */

@SuppressLint("Registered")
public class BaseActivity extends DaggerAppCompatActivity {

    protected static final String TAG = BaseActivity.class.getSimpleName();
    public void setBackButtonEnable(boolean enable) {
        if (getSupportActionBar() != null) {
            Drawable upArrow;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP)
                upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material, null);
            else
                upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);

            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            getSupportActionBar().setDisplayHomeAsUpEnabled(enable);
            getSupportActionBar().setDisplayShowHomeEnabled(enable);
        }
    }

    public void setTransparentToolbar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    public void removeToolbarShadow() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setElevation(0);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackButtonClicked();
        }
        return super.onOptionsItemSelected(item);
    }

    public void onBackButtonClicked() {
        if (getSupportFragmentManager().getBackStackEntryCount() >= 1)
            getSupportFragmentManager().popBackStack();
        else
            finish();
    }
}
