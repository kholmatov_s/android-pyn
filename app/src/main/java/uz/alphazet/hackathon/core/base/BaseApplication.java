package uz.alphazet.hackathon.core.base;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import timber.log.Timber;
import uz.alphazet.hackathon.core.di.AppComponent;
import uz.alphazet.hackathon.core.di.DaggerAppComponent;


public class BaseApplication extends DaggerApplication {

    /*private static BaseApplication baseApplication;

    public static BaseApplication getInstance() {
        return baseApplication;
    }*/

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        //baseApplication = this;
        Timber.plant(new Timber.DebugTree());
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        appComponent = DaggerAppComponent.builder()
                .application(this)
                .build();
        appComponent.inject(this);
        return appComponent;
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

}
