package uz.alphazet.hackathon.core.client;

/**
 * Created by Siyavushkhon on 01.12.2018.
 */

public interface APIItems {
    String BASE_URL = "http://aa634b9a.ngrok.io/api/";

    String CODE = "code";
    String MESSAGE = "message";
    String DATA = "data";

    String CATEGORIES = "categories";
    String FEATURES = "features";
    String PLACES = "places";
    String PLACE = "place";

    String ID = "id";
    String TITLE = "title";
    String FILE_URL = "file_url";
    String FEATURES_STR = "features_str";
    String REVIEWS_COUNT = "reviews_count";
    String ADDRESS = "address";
    String RATE = "rate";

}
