package uz.alphazet.hackathon.core.client;

/**
 * Created by Kholmatov Siyavushkhon (TuronTelecom) on 30.08.2018.
 */
public interface BaseInteractorListener {
    void onDataLoaded(ResponseData data);

    void onConnectionError(Throwable e);

    void onFailure(String message);
}
