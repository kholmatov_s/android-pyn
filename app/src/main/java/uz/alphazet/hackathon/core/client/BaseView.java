package uz.alphazet.hackathon.core.client;

/**
 * Created by Siyavushkhon on 02.12.2018.
 */

public interface BaseView {
    void showProgress();
    void hideProgress();
}
