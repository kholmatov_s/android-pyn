package uz.alphazet.hackathon.core.client;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import uz.alphazet.hackathon.core.client.models.Category;
import uz.alphazet.hackathon.core.client.models.Feature;
import uz.alphazet.hackathon.core.client.models.Place;

/**
 * Created by Siyavushkhon on 02.12.2018.
 */

public class Data implements APIItems {

    @SerializedName(CATEGORIES)
    private ArrayList<Category> categoryList;
    @SerializedName(FEATURES)
    private ArrayList<Feature> featureList;
    @SerializedName(PLACES)
    private ArrayList<Place> placeList;
    @SerializedName(PLACE)
    private Place place;


    public Place getPlace() {
        return place;
    }

    public ArrayList<Category> getCategoryList() {
        return categoryList;
    }

    public ArrayList<Feature> getFeatureList() {
        return featureList;
    }

    public ArrayList<Place> getPlaceList() {
        return placeList;
    }
}
