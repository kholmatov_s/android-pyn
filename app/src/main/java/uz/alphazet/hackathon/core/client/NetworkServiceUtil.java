package uz.alphazet.hackathon.core.client;

import javax.inject.Inject;

import retrofit2.Retrofit;

public class NetworkServiceUtil {

    private Retrofit retrofitClient;

    @Inject
    public NetworkServiceUtil(Retrofit retrofitClient) {
        this.retrofitClient = retrofitClient;
    }

    public <T> T getService(Class<T> tClass) {
        return retrofitClient.create(tClass);
    }
}
