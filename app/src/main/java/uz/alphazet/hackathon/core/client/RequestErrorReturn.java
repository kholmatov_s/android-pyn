package uz.alphazet.hackathon.core.client;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;

import java.io.IOException;

import io.reactivex.functions.Function;

/**
 * Created by Kholmatov Siyavushkhon (TuronTelecom) on 30.08.2018.
 */
public class RequestErrorReturn implements Function<Throwable, ResponseData> {

    private BaseInteractorListener listener;

    public RequestErrorReturn(BaseInteractorListener listener) {
        this.listener = listener;
    }

    @Override
    public ResponseData apply(Throwable throwable) throws Exception {
        if (throwable instanceof IOException || throwable instanceof HttpException || throwable instanceof NullPointerException) {
            if (listener != null)
                listener.onConnectionError(throwable);
        }
        return null;
    }

}
