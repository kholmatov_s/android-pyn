package uz.alphazet.hackathon.core.client;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Siyavushkhon on 02.12.2018.
 */

public class ResponseData implements APIItems {
    @SerializedName(CODE)
    private int code;
    @SerializedName(MESSAGE)
    private String message;
    @SerializedName(DATA)
    private Data data;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public Data getData() {
        return data;
    }
}
