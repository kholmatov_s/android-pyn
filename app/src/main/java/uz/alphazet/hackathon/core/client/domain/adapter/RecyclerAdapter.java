package uz.alphazet.hackathon.core.client.domain.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

import java.util.ArrayList;
import java.util.List;

import uz.alphazet.hackathon.R;
import uz.alphazet.hackathon.core.client.domain.viewholder.BaseViewHolder;
import uz.alphazet.hackathon.core.views.recyclerview.ColumnCounter;
import uz.alphazet.hackathon.core.views.recyclerview.ItemClickListener;


/**
 * Created by Kholmatov Siyavushkhon (TuronTelecom) on 12.07.2018.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private List<Object> list;
    private Object viewObject;
    private ItemClickListener itemClickListener;
    private Context context;

    private ColumnCounter columnCounter;
    private BaseViewHolder baseViewHolder;
    private int rowViewId = -1;
    private AdapterListener listener;
    protected RequestManager mGlide;

    public RecyclerAdapter(Context context, ColumnCounter columnCounter) {
        this.columnCounter = columnCounter;
        this.context = context;
        this.list = new ArrayList<>();
        this.mGlide = Glide.with(context);
    }

    public RecyclerAdapter(Context context, ColumnCounter columnCounter, int rowViewId) {
        this(context, columnCounter);
        this.rowViewId = rowViewId;
    }

    public void setOnItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public void setData(List<Object> newList) {
        if (list == null) {
            list = newList;
            if (viewObject == null)
                viewObject = list.get(0);
            notifyDataSetChanged();
        } else if (newList != null && newList.size() > 0) {
            list.addAll(newList);
            if (viewObject == null)
                viewObject = newList.get(0);
            notifyItemRangeChanged(getItemCount() + 1, newList.size());
        }
    }

    public void clearData() {
        int size = getItemCount();
        list.clear();
        notifyItemRangeRemoved(0, size);
    }

    /**
     * Обновляяем данные загружая другие страницы
     *
     * @param newData новый лист с данными
     */
    public void updateScrollData(List<Object> newData) {
        list.addAll(newData);
        notifyItemRangeChanged(getItemCount() + 1, newData.size());
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(rowViewId, parent, false);
        baseViewHolder = BaseViewHolder.getViewHolder(view, itemClickListener, columnCounter, viewObject);
        baseViewHolder.setGlide(mGlide);
        baseViewHolder.setSize(getItemCount());
        if (listener != null)
            listener.onAdapterViewCreated(baseViewHolder);
        return baseViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        if (list != null && list.size() > 0 && list.get(position) != null)
            holder.setValues(list.get(position));
    }

    @Override
    public void onViewRecycled(@NonNull BaseViewHolder holder) {
        holder.clearGlide();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setListener(AdapterListener listener) {
        this.listener = listener;
    }

    public interface AdapterListener {
        void onAdapterViewCreated(BaseViewHolder baseViewHolder);
    }
}
