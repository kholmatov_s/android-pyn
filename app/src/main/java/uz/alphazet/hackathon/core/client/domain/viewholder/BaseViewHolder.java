package uz.alphazet.hackathon.core.client.domain.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bumptech.glide.RequestManager;

import uz.alphazet.hackathon.core.client.models.Category;
import uz.alphazet.hackathon.core.client.models.Comment;
import uz.alphazet.hackathon.core.client.models.Feature;
import uz.alphazet.hackathon.core.client.models.Place;
import uz.alphazet.hackathon.core.views.recyclerview.ColumnCounter;
import uz.alphazet.hackathon.core.views.recyclerview.ItemClickListener;

/**
 * Created by Kholmatov Siyavushkhon (TuronTelecom) on 12.07.2018.
 */

public class BaseViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private ItemClickListener itemClickListener;
    private Object viewObject;
    private ColumnCounter columnCounter;
    protected RequestManager mGlide;
    private int size;

    public BaseViewHolder(View itemView, ItemClickListener itemClickListener, ColumnCounter columnCounter) {
        super(itemView);

        this.columnCounter = columnCounter;
        this.itemClickListener = itemClickListener;
        itemView.setOnClickListener(this);
    }

    public void setGlide(RequestManager mGlide) {
        this.mGlide = mGlide;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public static BaseViewHolder getViewHolder(View itemView, ItemClickListener itemClickListener, ColumnCounter columnCounter, Object object) {
        if (object instanceof Category)
            return new CategoryViewHolder(itemView, itemClickListener, columnCounter);
        else if (object instanceof Feature)
            return new FeatureViewHolder(itemView, itemClickListener, columnCounter);
        else if (object instanceof Place)
            return new PlacesViewHolder(itemView, itemClickListener, columnCounter);
        else if (object instanceof Comment)
            return new CommentViewHolder(itemView, itemClickListener, columnCounter);
        else
            return new BaseViewHolder(itemView, itemClickListener, columnCounter);
    }

    public void setValues(Object viewObject) {
        this.viewObject = viewObject;
    }

    public Object getViewObject() {
        return viewObject;
    }

    public void clearGlide() {
    }

    @Override
    public void onClick(View view) {
        if (itemClickListener != null) {
            itemClickListener.onItemClick(viewObject);
        }
    }
}
