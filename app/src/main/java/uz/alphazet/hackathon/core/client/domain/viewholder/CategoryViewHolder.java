package uz.alphazet.hackathon.core.client.domain.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import uz.alphazet.hackathon.R;
import uz.alphazet.hackathon.core.client.models.Category;
import uz.alphazet.hackathon.core.views.recyclerview.ColumnCounter;
import uz.alphazet.hackathon.core.views.recyclerview.ItemClickListener;

/**
 * Created by Siyavushkhon on 02.12.2018.
 */

public class CategoryViewHolder extends BaseViewHolder {

    private ImageView imageView;
    private TextView textView;

    public CategoryViewHolder(View itemView, ItemClickListener itemClickListener, ColumnCounter columnCounter) {
        super(itemView, itemClickListener, columnCounter);
        imageView = itemView.findViewById(R.id.imageView);
        textView = itemView.findViewById(R.id.textView);
    }

    @Override
    public void setValues(Object viewObject) {
        if (viewObject != null && viewObject instanceof Category) {
            Category category = (Category) viewObject;
            mGlide.load(category.getFileUrl()).into(imageView);
            textView.setText(category.getTitle());
        }
        super.setValues(viewObject);
    }
}
