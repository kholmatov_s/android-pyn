package uz.alphazet.hackathon.core.client.domain.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import uz.alphazet.hackathon.R;
import uz.alphazet.hackathon.core.client.models.Comment;
import uz.alphazet.hackathon.core.client.models.Place;
import uz.alphazet.hackathon.core.views.recyclerview.ColumnCounter;
import uz.alphazet.hackathon.core.views.recyclerview.ItemClickListener;

/**
 * Created by Siyavushkhon on 02.12.2018.
 */

public class CommentViewHolder extends BaseViewHolder {

    private TextView author, message;
    private RatingBar ratingBar;

    public CommentViewHolder(View itemView, ItemClickListener itemClickListener, ColumnCounter columnCounter) {
        super(itemView, itemClickListener, columnCounter);
        author = itemView.findViewById(R.id.feedbackAuthorName);
        message= itemView.findViewById(R.id.feedbackMessage);
        ratingBar = itemView.findViewById(R.id.ratingBar);
    }

    @Override
    public void setValues(Object viewObject) {
        if (viewObject != null && viewObject instanceof Comment) {
            Comment comment = (Comment) viewObject;
            author.setText(comment.getName());
            message.setText(comment.getText());
            ratingBar.setProgress(comment.getRating());
        }
        super.setValues(viewObject);
    }
}
