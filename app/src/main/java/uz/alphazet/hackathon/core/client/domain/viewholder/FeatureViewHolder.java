package uz.alphazet.hackathon.core.client.domain.viewholder;

import android.view.View;
import android.widget.TextView;

import uz.alphazet.hackathon.R;
import uz.alphazet.hackathon.core.client.models.Feature;
import uz.alphazet.hackathon.core.views.recyclerview.ColumnCounter;
import uz.alphazet.hackathon.core.views.recyclerview.ItemClickListener;

/**
 * Created by Siyavushkhon on 02.12.2018.
 */

public class FeatureViewHolder extends BaseViewHolder {

    private TextView textView;

    public FeatureViewHolder(View itemView, ItemClickListener itemClickListener, ColumnCounter columnCounter) {
        super(itemView, itemClickListener, columnCounter);
        textView = itemView.findViewById(R.id.textView);
    }

    @Override
    public void setValues(Object viewObject) {
        if (viewObject != null && viewObject instanceof Feature) {
            Feature feature = (Feature) viewObject;
            textView.setText(feature.getTitle());
        }
        super.setValues(viewObject);
    }
}
