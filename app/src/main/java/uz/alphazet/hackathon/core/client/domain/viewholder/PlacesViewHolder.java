package uz.alphazet.hackathon.core.client.domain.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import uz.alphazet.hackathon.R;
import uz.alphazet.hackathon.core.client.models.Feature;
import uz.alphazet.hackathon.core.client.models.Place;
import uz.alphazet.hackathon.core.views.recyclerview.ColumnCounter;
import uz.alphazet.hackathon.core.views.recyclerview.ItemClickListener;

/**
 * Created by Siyavushkhon on 02.12.2018.
 */

public class PlacesViewHolder extends BaseViewHolder {

    private TextView nameTv, reviewsTv, tagTv, addressTv;
    private RatingBar ratingBar;
    private ImageView imageView;

    public PlacesViewHolder(View itemView, ItemClickListener itemClickListener, ColumnCounter columnCounter) {
        super(itemView, itemClickListener, columnCounter);
        nameTv = itemView.findViewById(R.id.nameTV);
        reviewsTv = itemView.findViewById(R.id.reviewsTV);
        tagTv = itemView.findViewById(R.id.tagTV);
        addressTv = itemView.findViewById(R.id.addressTV);
        ratingBar = itemView.findViewById(R.id.ratingBar);
        imageView = itemView.findViewById(R.id.imageView);
    }

    @Override
    public void setValues(Object viewObject) {
        if (viewObject != null && viewObject instanceof Place) {
            Place place = (Place) viewObject;
            nameTv.setText(place.getTitle());
            reviewsTv.setText(place.getCount() + " отзывов");
            tagTv.setText(place.getTags());
            addressTv.setText(place.getAddress());
            ratingBar.setProgress(place.getRate());
            mGlide.load(place.getPoster()).into(imageView);
        }
        super.setValues(viewObject);
    }
}
