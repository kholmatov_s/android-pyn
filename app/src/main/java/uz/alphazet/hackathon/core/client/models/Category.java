package uz.alphazet.hackathon.core.client.models;

import com.google.gson.annotations.SerializedName;

import uz.alphazet.hackathon.core.client.APIItems;

/**
 * Created by Siyavushkhon on 02.12.2018.
 */

public class Category implements APIItems {
    @SerializedName(ID)
    private int id;
    @SerializedName(TITLE)
    private String title;
    @SerializedName(FILE_URL)
    private String fileUrl;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getFileUrl() {
        return fileUrl;
    }
}
