package uz.alphazet.hackathon.core.client.models;

import com.google.gson.annotations.SerializedName;

import uz.alphazet.hackathon.core.client.APIItems;

/**
 * Created by Siyavushkhon on 02.12.2018.
 */

public class Comment implements APIItems {
    @SerializedName("name")
    private String name;
    @SerializedName("text")
    private String text;
    @SerializedName(RATE)
    private int rating;

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }

    public int getRating() {
        return rating;
    }
}
