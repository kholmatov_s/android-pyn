package uz.alphazet.hackathon.core.client.models;

import com.google.gson.annotations.SerializedName;

import uz.alphazet.hackathon.core.client.APIItems;

/**
 * Created by Siyavushkhon on 02.12.2018.
 */

public class Feature implements APIItems {
    @SerializedName(ID)
    private int id;
    @SerializedName(TITLE)
    private String title;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }
}
