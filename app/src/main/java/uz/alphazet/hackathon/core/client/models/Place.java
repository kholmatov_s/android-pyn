package uz.alphazet.hackathon.core.client.models;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

import java.util.ArrayList;
import java.util.List;

import uz.alphazet.hackathon.core.client.APIItems;

/**
 * Created by Siyavushkhon on 02.12.2018.
 */

public class Place implements APIItems {
    @SerializedName(ID)
    private int id;
    @SerializedName(TITLE)
    private String title;
    @SerializedName(FILE_URL)
    private String poster;
    @SerializedName(FEATURES_STR)
    private String tags;
    @SerializedName(RATE)
    private int rate;
    @SerializedName(REVIEWS_COUNT)
    private int count;
    @SerializedName(ADDRESS)
    private String address;
    @SerializedName("lat")
    private double mapLat;
    @SerializedName("long")
    private double mapLong;
    @SerializedName("website_url")
    private String websiteUrl;
    @SerializedName("phone")
    private String phoneNumber;
    @SerializedName("reviews")
    private ArrayList<Comment> commentList;

    public ArrayList<Comment> getCommentList() {
        return commentList;
    }

    public double getMapLat() {
        return mapLat;
    }

    public double getMapLong() {
        return mapLong;
    }

    public String getWebsiteUrl() {
        return websiteUrl;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPoster() {
        return poster;
    }

    public String getAddress() {
        return address;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getTags() {
        return tags;
    }

    public int getRate() {
        return rate;
    }

    public int getCount() {
        return count;
    }
}
