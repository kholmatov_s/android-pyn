package uz.alphazet.hackathon.core.client.services;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import uz.alphazet.hackathon.core.client.ResponseData;

/**
 * Created by Siyavushkhon on 02.12.2018.
 */

public interface MainService {

    @GET("categories/main/list")
    Single<ResponseData> getCategoriesList();

    @GET("categories/features/list")
    Single<ResponseData> getFeatureList(@Query("category_id") int catId);

    @GET("places/main/list")
    Single<ResponseData> getPlacesList(@Query("feature_id") int featureId);

    @GET("places/main/show/{placeId}")
    Single<ResponseData> getPlacesDetail(@Path("placeId") int placeId);


}
