package uz.alphazet.hackathon.core.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import uz.alphazet.hackathon.core.di.modules.main.FeatureListModule;
import uz.alphazet.hackathon.core.di.modules.main.MainActivityModule;
import uz.alphazet.hackathon.core.di.modules.main.MainFragmentProvider;
import uz.alphazet.hackathon.ui.presentation.main.DetailActivity;
import uz.alphazet.hackathon.ui.presentation.main.DetailActivity_;
import uz.alphazet.hackathon.ui.presentation.main.FeatureListActivity_;
import uz.alphazet.hackathon.ui.presentation.main.MainActivity_;
import uz.alphazet.hackathon.ui.presentation.main.PlacesListActivity;
import uz.alphazet.hackathon.ui.presentation.main.PlacesListActivity_;

/**
 * Created by Siyavushkhon on 01.12.2018.
 */

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = {
            MainActivityModule.class,
            MainFragmentProvider.class
    })
    abstract MainActivity_ provideMainActivity();

    @ContributesAndroidInjector(modules = {
            FeatureListModule.class
    })
    abstract FeatureListActivity_ provideCategoryListActivity();

    @ContributesAndroidInjector
    abstract PlacesListActivity_ providePlacesListActivity();

    @ContributesAndroidInjector
    abstract DetailActivity_ provideDetailActivity();
}
