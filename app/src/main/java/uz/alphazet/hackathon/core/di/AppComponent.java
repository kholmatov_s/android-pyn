package uz.alphazet.hackathon.core.di;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import dagger.android.support.AndroidSupportInjectionModule;
import uz.alphazet.hackathon.core.base.BaseApplication;

/**
 * Created by Siyavushkhon on 01.12.2018.
 */

@Singleton
@Component(modules =  {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        ActivityBuilder.class
})

public interface AppComponent extends AndroidInjector<DaggerApplication> {

    void inject(BaseApplication baseApplication);

    @Override
    void inject(DaggerApplication instance);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }
}
