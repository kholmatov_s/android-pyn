package uz.alphazet.hackathon.core.di;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import uz.alphazet.hackathon.core.di.modules.network.GlideModule;
import uz.alphazet.hackathon.core.di.modules.network.RetrofitModule;

/**
 * Created by Siyavushkhon on 01.12.2018.
 */

@Module(includes = {
        RetrofitModule.class,
        GlideModule.class
})
public abstract class AppModule {


    @Binds
    @Singleton
    abstract Context bindContext(Application application);
}
