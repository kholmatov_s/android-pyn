package uz.alphazet.hackathon.core.di.modules.main;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import uz.alphazet.hackathon.core.di.modules.main.fragments.DiscoveryFragmentModule;
import uz.alphazet.hackathon.ui.presentation.main.fragment.discovery.DiscoveryFragment_;

/**
 * Created by Siyavushkhon on 02.12.2018.
 */

@Module
public abstract class MainFragmentProvider {
    @ContributesAndroidInjector(modules = {
            DiscoveryFragmentModule.class
    })
    abstract DiscoveryFragment_ provideDiscoveryFragmentFactory();
}
