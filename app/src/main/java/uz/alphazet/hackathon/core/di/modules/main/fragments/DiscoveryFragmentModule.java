package uz.alphazet.hackathon.core.di.modules.main.fragments;

import dagger.Binds;
import dagger.Module;
import uz.alphazet.hackathon.ui.presentation.main.fragment.discovery.DiscoveryContract;
import uz.alphazet.hackathon.ui.presentation.main.fragment.discovery.DiscoveryFragment;
import uz.alphazet.hackathon.ui.presentation.main.fragment.discovery.DiscoveryFragment_;

/**
 * Created by Siyavushkhon on 02.12.2018.
 */

@Module
public abstract class DiscoveryFragmentModule {

    @Binds
    abstract DiscoveryContract.View bindDiscoveryFragmentView(DiscoveryFragment_ discoveryFragment);
}
