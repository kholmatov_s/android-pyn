package uz.alphazet.hackathon.core.di.modules.network;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public abstract class GlideModule {
    @Provides
    @Singleton
    public static RequestManager provideGlide(Context context) {
        return Glide.with(context);
    }
}
