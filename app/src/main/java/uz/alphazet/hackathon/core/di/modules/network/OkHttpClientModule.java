package uz.alphazet.hackathon.core.di.modules.network;

import android.content.Context;

import java.io.File;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import timber.log.Timber;

/**
 * Created by Kholmatov Siyavushkhon (TuronTelecom) on 28.06.2018.
 */

@Module
public abstract class OkHttpClientModule {
    @Provides
    @Singleton
    public static OkHttpClient okHttpClient(Cache cache, HttpLoggingInterceptor httpLoggingInterceptor) {
        return new OkHttpClient()
                .newBuilder()
                .cache(cache)
                .addInterceptor(httpLoggingInterceptor)
                .build();
    }

    @Provides
    @Singleton
    public static Cache cache(File cacheFile) {
        return new Cache(cacheFile, 10 * 1000 * 1000); //10 MB
    }

    @Provides
    @Singleton
    public static File file(Context context) {
        File file = new File(context.getCacheDir(), "HttpCache");
        file.mkdirs();
        return file;
    }

    /*@Provides
    @Singleton
    public static Interceptor customInterceptor(final PreferencesUtil preferencesUtil, final NetworkUtils networkUtils, final UserDaoImpl userDao) {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                long timestamp = System.currentTimeMillis() / 1000;

                Request.Builder builder = request.newBuilder()
                        .addHeader(APIItems.LANG, preferencesUtil.getLanguage())
                        .addHeader(APIItems.EXPIRED_DATE, String.valueOf(timestamp))
                        .addHeader(APIItems.DEVICE_ID, networkUtils.generateDeviceId());

                if (preferencesUtil.isAuthorised()) {
                    User user = userDao.getUserDirectly();
                    String loginData = (user != null) ? user.getAccessKey() : null;
                    if (loginData != null && !loginData.isEmpty())
                        builder.addHeader(APIItems.ACCESS_KEY, networkUtils.generateAccessKey(request.url().toString(), timestamp, loginData))
                                .addHeader(APIItems.LOGIN_DATA, loginData);

                } else
                    builder.addHeader(APIItems.ACCESS_KEY, networkUtils.generateAccessKey(request.url().toString(), timestamp, null));

                return chain.proceed(builder.build());
            }
        };
    }*/

    @Provides
    @Singleton
    public static HttpLoggingInterceptor httpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Timber.d(message);
            }
        });
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }
}
