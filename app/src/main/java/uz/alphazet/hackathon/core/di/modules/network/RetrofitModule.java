package uz.alphazet.hackathon.core.di.modules.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uz.alphazet.hackathon.core.client.APIItems;

@Module(includes = OkHttpClientModule.class)
public abstract class RetrofitModule {

    @Provides
    @Singleton
    public static Retrofit provideRetrofit(OkHttpClient okHttpClient, RxJava2CallAdapterFactory rxJavaCallAdapter, GsonConverterFactory gsonConverterFactory) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(APIItems.BASE_URL)
                .addCallAdapterFactory(rxJavaCallAdapter)
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

    @Provides
    @Singleton
    public static Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    public static GsonConverterFactory provideGsonConverterFactory(Gson gson) {
        return GsonConverterFactory.create(gson);
    }

    @Provides
    @Singleton
    public static RxJava2CallAdapterFactory provideRxJavaCallAdapter() {
        return RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io());
    }
}
