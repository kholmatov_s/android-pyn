package uz.alphazet.hackathon.core.views.recyclerview;

public interface ColumnCounter {
    int getColumnNumber();
}
