package uz.alphazet.hackathon.core.views.recyclerview;

/**
 * Created by Kholmatov Siyavushkhon (TuronTelecom) on 12.07.2018.
 */

public interface ItemClickListener {
    void onItemClick(Object object);
}
