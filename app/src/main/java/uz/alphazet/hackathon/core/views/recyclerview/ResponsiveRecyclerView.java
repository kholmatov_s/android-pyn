package uz.alphazet.hackathon.core.views.recyclerview;

import android.content.Context;
import android.content.res.Configuration;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;


/**
 * Created by Kholmatov Siyavushkhon (TuronTelecom) on 12.07.2018.
 */

public class ResponsiveRecyclerView extends RecyclerView implements ColumnCounter {

    private static final int SMALL = 0;
    private static final int BIG = 1;

    private GridLayoutManager gridLayoutManager;
    private Context context;
    private RecyclerViewScrollListener scrollListener;

    private int columnNumber = 2;
    private int contentType = BIG;

    private int currentPage = 0, previousTotalItemCount = 0, startingPageIndex = 0, visibleThreshold = 2;
    private boolean loading = true, isEndlessScrollable = true;
    private boolean errorDialogShown = false;

    public ResponsiveRecyclerView(Context context) {
        super(context);
        setup(context, null);
    }

    public ResponsiveRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setup(context, attrs);
    }

    public ResponsiveRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setup(context, attrs);
    }

    private void setup(Context context, AttributeSet attrs) {
        if (isInEditMode())
            return;
        this.context = context;
        notifyColumns();
    }

    public void setScrollListener(RecyclerViewScrollListener scrollListener) {
        this.scrollListener = scrollListener;
    }

    public void setContentTypeBig() {
        contentType = BIG;
        notifyColumns();
    }

    public void setContentTypeSmall() {
        contentType = SMALL;
        notifyColumns();
    }

    public void setPaginationEnable(boolean paginationEnable) {
        isEndlessScrollable = paginationEnable;
    }

    @Override
    public void onScrolled(int dx, int dy) {
        super.onScrolled(dx, dy);
        if (scrollListener != null && getLayoutManager() != null) {
            if (dy >= 0) {
                if (dy > 0)
                    scrollListener.hideFab();
                if (isEndlessScrollable)
                    onEndlessScroll();
            } else {
                scrollListener.showFab();
            }
        }
    }

    public void onLoadFailed() {
        if (loading)
            loading = false;
    }

    private void onEndlessScroll() {
        int lastVisibleItemPosition = 0;
        int totalItemCount = getLayoutManager().getItemCount();

        if (getLayoutManager() instanceof GridLayoutManager) {
            lastVisibleItemPosition = ((GridLayoutManager) getLayoutManager()).findLastVisibleItemPosition();
        } else if (getLayoutManager() instanceof LinearLayoutManager) {
            lastVisibleItemPosition = ((LinearLayoutManager) getLayoutManager()).findLastVisibleItemPosition();
        }

        if (totalItemCount < previousTotalItemCount) {
            this.currentPage = this.startingPageIndex;
            this.previousTotalItemCount = totalItemCount;
            if (totalItemCount == 0) {
                this.loading = true;
            }
        }
        if (loading && (totalItemCount > previousTotalItemCount)) {
            loading = false;
            previousTotalItemCount = totalItemCount;
        }

        if (!loading && (lastVisibleItemPosition + visibleThreshold) > totalItemCount) {
            if (currentPage == 0)
                currentPage++;
            this.scrollListener.loadData();
            loading = true;
        }
    }

    @Override
    public void setLayoutManager(LayoutManager layout) {
        super.setLayoutManager(layout);
        if (layout instanceof GridLayoutManager) {
            gridLayoutManager = (GridLayoutManager) layout;
        }
    }

    @Override
    protected void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        notifyColumns();
        if (getAdapter() != null)
            getAdapter().notifyDataSetChanged();
    }

    private void notifyColumns() {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            if (contentType == BIG) {
                if (isTablet()) {
                    columnNumber = 4;
                } else {
                    columnNumber = 2;
                }
            } else if (contentType == SMALL) {
                if (isTablet()) {
                    columnNumber = 6;
                } else {
                    columnNumber = 3;
                }
            }
        } else {
            if (contentType == BIG) {
                if (isTablet()) {
                    columnNumber = 6;
                } else {
                    columnNumber = 4;
                }
            } else if (contentType == SMALL) {
                if (isTablet()) {
                    columnNumber = 8;
                } else {
                    columnNumber = 6;
                }
            }
        }
        if (gridLayoutManager != null)
            gridLayoutManager.setSpanCount(columnNumber);
    }

    public boolean isTablet() {
        boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
        boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return (xlarge || large);
    }

    @Override
    public int getColumnNumber() {
        return columnNumber;
    }

    public interface RecyclerViewScrollListener {
        void loadData();

        void showFab();

        void hideFab();
    }
}
