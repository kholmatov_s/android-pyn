package uz.alphazet.hackathon.ui.presentation.main;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Locale;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import uz.alphazet.hackathon.R;
import uz.alphazet.hackathon.core.base.BaseActivity;
import uz.alphazet.hackathon.core.client.BaseView;
import uz.alphazet.hackathon.core.client.NetworkServiceUtil;
import uz.alphazet.hackathon.core.client.ResponseData;
import uz.alphazet.hackathon.core.client.domain.adapter.RecyclerAdapter;
import uz.alphazet.hackathon.core.client.models.Place;
import uz.alphazet.hackathon.core.client.services.MainService;
import uz.alphazet.hackathon.core.views.recyclerview.ItemClickListener;
import uz.alphazet.hackathon.core.views.recyclerview.ResponsiveRecyclerView;

@SuppressLint("Registered")
@EActivity(R.layout.activity_detail)
public class DetailActivity extends BaseActivity implements BaseView {

    @Inject
    NetworkServiceUtil networkServiceUtil;

    @Extra
    String title, poster;
    @Extra
    int placeId;

    @ViewById
    Toolbar toolbar;
    @ViewById
    CollapsingToolbarLayout toolbar_layout;
    @ViewById
    ImageView imageView;
    @ViewById
    ProgressBar progressBar;
    @ViewById
    TextView nameTV, tagTV, addressTV, reviewsTV, phoneTV, websiteTV;
    @ViewById
    RatingBar ratingBar;
    @ViewById
    LinearLayout infoLayout;
    @ViewById
    ResponsiveRecyclerView reviewsRV;


    private MainService mainService;
    private RecyclerAdapter recyclerAdapter;
    private Place place;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        mainService = networkServiceUtil.getService(MainService.class);
    }

    @AfterViews
    void afterViews() {
        Glide.with(this).load(poster).into(imageView);
        reviewsRV.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        reviewsRV.setPaginationEnable(false);
        recyclerAdapter = new RecyclerAdapter(this, reviewsRV, R.layout.feedback);
        reviewsRV.setAdapter(recyclerAdapter);
        reviewsRV.setNestedScrollingEnabled(false);
        toolbar_layout.setTitle(title);
        setBackButtonEnable(true);
        setTitle(title);
        loadData(placeId);
    }


    private void initUI(Place place) {
        this.place = place;
        nameTV.setText(place.getTitle());
        tagTV.setText(place.getTags());
        addressTV.setText(place.getAddress());
        reviewsTV.setText(place.getCount() + " отзыв(a)");
        ratingBar.setProgress(place.getRate());
        phoneTV.setText("Позвонить " + place.getPhoneNumber());
        websiteTV.setText(place.getWebsiteUrl());
        ArrayList list = place.getCommentList();
        recyclerAdapter.setData(list);
    }


    private void loadData(int placeId) {
        if (mainService != null) {
            mainService.getPlacesDetail(placeId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableSingleObserver<ResponseData>() {
                        @Override
                        public void onSuccess(ResponseData responseData) {
                            if (responseData != null && responseData.getData() != null && responseData.getData().getPlace() != null) {
                                initUI(responseData.getData().getPlace());
                            }
                            hideProgress();
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            hideProgress();
                        }
                    });
        }
    }

    @Click(R.id.fab)
    void onFabMapClick() {
        String uri = String.format(Locale.ENGLISH, "geo:%f,%f", place.getMapLat(), place.getMapLong());
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        infoLayout.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
        infoLayout.setVisibility(View.VISIBLE);
    }

}
