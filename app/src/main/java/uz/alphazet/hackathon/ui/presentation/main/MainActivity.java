package uz.alphazet.hackathon.ui.presentation.main;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import uz.alphazet.hackathon.R;
import uz.alphazet.hackathon.core.base.BaseActivity;
import uz.alphazet.hackathon.ui.presentation.main.fragment.discovery.DiscoveryFragment_;

@SuppressLint("Registered")
@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    @ViewById
    BottomNavigationView navigation;
    private FragmentManager fragmentManager;

    @AfterViews
    void afterViews() {
        navigation.setOnNavigationItemSelectedListener(this);
        fragmentManager = getSupportFragmentManager();
        openFragment(DiscoveryFragment_.builder().build(), false);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == navigation.getSelectedItemId())
            return false;
        switch (item.getItemId()) {
            case R.id.nav_discovery:
                openFragment(DiscoveryFragment_.builder().build(), false);
                return true;
        }
        return false;
    }

    public void openFragment(Fragment fragment, boolean addBackStack) {
        if (fragmentManager != null) {
            fragmentManager.beginTransaction()
                    .add(R.id.container, fragment)
                    .commitAllowingStateLoss();
        }
    }
}
