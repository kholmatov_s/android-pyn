package uz.alphazet.hackathon.ui.presentation.main;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ProgressBar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import uz.alphazet.hackathon.R;
import uz.alphazet.hackathon.core.base.BaseActivity;
import uz.alphazet.hackathon.core.client.BaseView;
import uz.alphazet.hackathon.core.client.NetworkServiceUtil;
import uz.alphazet.hackathon.core.client.ResponseData;
import uz.alphazet.hackathon.core.client.domain.adapter.RecyclerAdapter;
import uz.alphazet.hackathon.core.client.models.Place;
import uz.alphazet.hackathon.core.client.services.MainService;
import uz.alphazet.hackathon.core.views.recyclerview.ItemClickListener;
import uz.alphazet.hackathon.core.views.recyclerview.ResponsiveRecyclerView;

@SuppressLint("Registered")
@EActivity(R.layout.activity_places_list)
public class PlacesListActivity extends BaseActivity implements BaseView, ItemClickListener {

    @Inject
    NetworkServiceUtil networkServiceUtil;

    @Extra
    int featureId;
    @Extra
    String featureName;

    @ViewById
    ProgressBar progressBar;
    @ViewById
    ResponsiveRecyclerView placesRV;

    private MainService mainService;
    private RecyclerAdapter recyclerAdapter;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainService = networkServiceUtil.getService(MainService.class);
        setTitle(featureName);
        setBackButtonEnable(true);
    }

    @AfterViews
    void afterViews() {
        placesRV.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL, false));
        recyclerAdapter = new RecyclerAdapter(this, placesRV, R.layout.row_places_item);
        placesRV.setPaginationEnable(false);
        recyclerAdapter.setOnItemClickListener(this);
        loadData();
    }

    private void loadData() {
        if (mainService != null) {
            mainService.getPlacesList(featureId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableSingleObserver<ResponseData>() {
                        @Override
                        public void onSuccess(ResponseData responseData) {
                            if (responseData != null && responseData.getData() != null && responseData.getData().getPlaceList() != null) {
                                ArrayList list = responseData.getData().getPlaceList();
                                recyclerAdapter.setData(list);
                                placesRV.setAdapter(recyclerAdapter);
                            }
                            hideProgress();
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            hideProgress();
                        }
                    });
        }
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(Object object) {
        Place place = (Place) object;
        DetailActivity_.intent(this)
                .title(place.getTitle())
                .placeId(place.getId())
                .poster(place.getPoster())
                .start()
                .withAnimation(android.R.anim.fade_in, android.R.anim.fade_out);
    }

}
