package uz.alphazet.hackathon.ui.presentation.main.fragment.discovery;

import java.util.ArrayList;

import uz.alphazet.hackathon.core.client.BaseInteractorListener;
import uz.alphazet.hackathon.core.client.BaseView;

/**
 * Created by Siyavushkhon on 02.12.2018.
 */

public interface DiscoveryContract {
    interface View extends BaseView {
        void onDataLoaded(ArrayList list);
    }

    interface Presenter {
        void loadData();
    }

    interface Interactor {
        void loadData();
        void setListener(Listener listener);
        interface Listener extends BaseInteractorListener {

        }
    }
}
