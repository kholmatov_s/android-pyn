package uz.alphazet.hackathon.ui.presentation.main.fragment.discovery;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ProgressBar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import uz.alphazet.hackathon.R;
import uz.alphazet.hackathon.core.client.domain.adapter.RecyclerAdapter;
import uz.alphazet.hackathon.core.client.models.Category;
import uz.alphazet.hackathon.core.views.recyclerview.ItemClickListener;
import uz.alphazet.hackathon.core.views.recyclerview.ResponsiveRecyclerView;
import uz.alphazet.hackathon.ui.presentation.main.FeatureListActivity_;

/**
 * Created by Siyavushkhon on 02.12.2018.
 */

@EFragment(R.layout.fragment_discovery)
public class DiscoveryFragment extends DaggerFragment implements DiscoveryContract.View, ItemClickListener {

    @Inject
    DiscoveryPresenterImpl discoveryPresenter;

    @ViewById
    ResponsiveRecyclerView categoriesRV;
    @ViewById
    ProgressBar progressBar;

    private RecyclerAdapter categoriesAdapter;

    @AfterViews
    void afterViews() {
        initView();
        if (discoveryPresenter != null) {
            discoveryPresenter.loadData();
        }
    }

    private void initView() {
        categoriesRV.setLayoutManager(new GridLayoutManager(getContext(), 3, LinearLayoutManager.VERTICAL, false));
        categoriesAdapter = new RecyclerAdapter(getContext(), categoriesRV, R.layout.row_discovery_category);
        categoriesAdapter.setOnItemClickListener(this);
        categoriesRV.setPaginationEnable(false);
        categoriesRV.setAdapter(categoriesAdapter);
        categoriesRV.setNestedScrollingEnabled(false);
    }


    @Override
    public void onDataLoaded(ArrayList list) {
        if (categoriesAdapter != null) {
            categoriesAdapter.setData(list);
        }
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(Object object) {
        if (object != null && object instanceof Category) {
            Category category = (Category) object;
            FeatureListActivity_.intent(this)
                    .categoryId(category.getId())
                    .start()
                    .withAnimation(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }
}
