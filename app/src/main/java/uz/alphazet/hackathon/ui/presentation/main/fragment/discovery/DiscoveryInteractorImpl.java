package uz.alphazet.hackathon.ui.presentation.main.fragment.discovery;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import uz.alphazet.hackathon.core.client.NetworkServiceUtil;
import uz.alphazet.hackathon.core.client.RequestErrorReturn;
import uz.alphazet.hackathon.core.client.ResponseData;
import uz.alphazet.hackathon.core.client.services.MainService;

/**
 * Created by Siyavushkhon on 02.12.2018.
 */

public class DiscoveryInteractorImpl implements DiscoveryContract.Interactor {

    private MainService mainService;
    private Listener listener;
    @Inject
    public DiscoveryInteractorImpl(NetworkServiceUtil networkServiceUtil) {
        this.mainService = networkServiceUtil.getService(MainService.class);
    }

    @Override
    public void loadData() {
        if (mainService != null) {
            mainService.getCategoriesList()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .onErrorReturn(new RequestErrorReturn(listener))
                    .subscribe(new DisposableSingleObserver<ResponseData>() {
                        @Override
                        public void onSuccess(ResponseData responseData) {
                            if (listener != null && responseData != null) {
                                if (responseData.getData() != null && responseData.getData().getCategoryList() != null) {
                                    listener.onDataLoaded(responseData);
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            if (listener != null) {
                                listener.onFailure(e.getLocalizedMessage());
                            }
                        }
                    });
        }
    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
    }
}
