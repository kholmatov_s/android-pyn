package uz.alphazet.hackathon.ui.presentation.main.fragment.discovery;

import javax.inject.Inject;

import uz.alphazet.hackathon.core.client.ResponseData;

/**
 * Created by Siyavushkhon on 02.12.2018.
 */

public class DiscoveryPresenterImpl implements DiscoveryContract.Presenter, DiscoveryContract.Interactor.Listener {

    private DiscoveryContract.View view;
    private DiscoveryContract.Interactor interactor;

    @Inject
    public DiscoveryPresenterImpl(DiscoveryContract.View view, DiscoveryInteractorImpl interactor) {
        this.view = view;
        this.interactor = interactor;
        this.interactor.setListener(this);
    }

    @Override
    public void loadData() {
        if (interactor != null && view != null) {
            view.showProgress();
            interactor.loadData();
        }
    }

    @Override
    public void onDataLoaded(ResponseData data) {
        if (view != null) {
            view.hideProgress();
            view.onDataLoaded(data.getData().getCategoryList());
        }
    }

    @Override
    public void onConnectionError(Throwable e) {
        if (view != null) {
            view.hideProgress();
        }
    }

    @Override
    public void onFailure(String message) {
        if (view != null) {
            view.hideProgress();
        }
    }

}
